package com.patientTest.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patientTest.model.PatientModel;
import com.patientTest.repository.PatientRepository;


@Service
public class PatientService {
	
	@Autowired
	PatientRepository patientRepository;
	
	public PatientModel save(PatientModel pat) {
		return patientRepository.save(pat);
	}

	
	public List<PatientModel> findAll(){
		return patientRepository.findAll();
	}
	
	
	public PatientModel findOne(Long patid) {
		return patientRepository.findOne(patid);
	}
	
	
	public void delete(PatientModel pat) {
		patientRepository.delete(pat);
	}
}
