package com.patientTest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.patientTest.dao.PatientService;
import com.patientTest.model.PatientModel;



@RestController
@RequestMapping("/patientdetails")
public class PatientController {
	@Autowired
	PatientService 	patientService;
	
	@PostMapping("/patient")
	public PatientModel createPatient(@Valid @RequestBody PatientModel pat) {
		return patientService.save(pat);
		
	}
	@GetMapping("/patient")
	public List<PatientModel> getAllPatiens(){
		return patientService.findAll();
	}
	
	@GetMapping("/patient/{id}")
	public ResponseEntity<PatientModel> getPatientById(@PathVariable(value="id") Long patid){
		PatientModel pat = patientService.findOne(patid);	
		if(pat==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(pat);
		
	}
	@PutMapping("/patient/{id}")
	public ResponseEntity<PatientModel> updatePatient(@PathVariable(value="id") Long patid,@Valid @RequestBody PatientModel patDetails){
		
		PatientModel pat=patientService.findOne(patid);
		if(pat==null) {
			return ResponseEntity.notFound().build();
		}
		
		pat.setPatientfirstname(patDetails.getPatientfirstname());
		pat.setPatientlastname(patDetails.getPatientlastname());
		pat.setAddress(patDetails.getAddress());
		pat.setPhonenumber(patDetails.getPhonenumber());
		
		PatientModel updatePatient=patientService.save(pat);
		return ResponseEntity.ok().body(updatePatient);
	
	}
	@DeleteMapping("/patient/{id}")
	public ResponseEntity<PatientModel> deletePatient(@PathVariable(value="id") Long patid){
		
		PatientModel pat=patientService.findOne(patid);
		if(pat==null) {
			return ResponseEntity.notFound().build();
		}
		patientService.delete(pat);
		
		return ResponseEntity.ok().build();
		
		
	}
	
	

}
